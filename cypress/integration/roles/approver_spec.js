import * as locators from '../../support/locators';

describe('EPMEDUTOUR-1605 | Verify the restrictions for user with Approver role', () => {

    before('setUp', () => {
        cy.login('approver');
    })

    it('test inability UI elements for approver', () => {

        // Tools
        cy.get(locators.MENU_LINKS[4]).click();
        cy.xpath("//a[@href='#/tools/import']/ancestor::li[1]").should('have.attr', 'aria-disabled')
            .and('equal', 'true');

        // Users
        /*cy.xpath("//a[@href='#/users']/ancestor::li[1]").should('have.attr', 'aria-disabled')
            .and('equal', 'true');*/
    })

})