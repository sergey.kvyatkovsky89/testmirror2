import {
    openObjects, setStatus
} from '../support/object.js';
import * as locators from '../support/locators';

describe('EPMEDUTOUR-804 | Verify that objects list is filtered by status', () => {

    const STATUS_DRAFT = 'Черновик';
    const STATUS_ARCHIVE = 'Архив';

    before('setUp', () => {

        cy.login('admin')
        openObjects();
        setStatus(STATUS_DRAFT);
    })

    it('test filtering of Objects list by statuses', () => {

        const statusLocator = '.ant-tag';

        cy.get(statusLocator).each((item) => {
            cy.wrap(item).should('contain.text', STATUS_DRAFT)
        });

        cy.get(locators.STATUS_ITEM).contains(STATUS_ARCHIVE).click();

        cy.get(statusLocator).each((item) => {
            cy.wrap(item).invoke('text').should('be.oneOf', [STATUS_DRAFT, STATUS_ARCHIVE]);
        });

        cy.get(':nth-child(1) > .ant-select-selection-item > .ant-select-selection-item-remove')
            .click();

        cy.get(statusLocator).each((item) => {
            cy.wrap(item).should('contain.text', STATUS_ARCHIVE);
        });
    })

})