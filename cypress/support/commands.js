import 'cypress-file-upload';

Cypress.Commands.add('login', (role) => {

    var testUsername, testPassword

    // get the test user
    const testUser = Cypress.env(role)
    testUsername = testUser.username
    testPassword = testUser.password

    cy.log('testUsername - ' + testUsername)

    cy.visit('/')
    cy.get('amplify-sign-in.hydrated').shadow()
        .find('input#username').type(testUsername)
    cy.get('amplify-sign-in.hydrated').shadow()
        .find('input#password').should('be.enabled').as('passInput')

    cy.get('@passInput').click({ force: true })
    cy.get('@passInput').type(testPassword, { force: true })

    cy.get('amplify-sign-in.hydrated').shadow()
        .find('div.sign-in-form-footer').find('slot[name=primary-footer-content]')
        .find('span').click() 

    cy.get('.radzima', { timeout: 20000 }).should('be.visible');
})

Cypress.Commands.add('loginProd', (role) => {

    var testUsername, testPassword

    // get the test user
    const testUser = Cypress.env(role)
    testUsername = testUser.username
    testPassword = testUser.password

    cy.log('testUsername - ' + testUsername)

    cy.visit('https://prod.d86eq06y04xkl.amplifyapp.com/#/')
    cy.get('amplify-sign-in.hydrated').shadow()
        .find('input#username').type(testUsername)
    cy.get('amplify-sign-in.hydrated').shadow()
        .find('input#password').should('be.enabled').as('passInput')

    cy.get('@passInput').click({ force: true })
    cy.get('@passInput').type(testPassword, { force: true })

    cy.get('amplify-sign-in.hydrated').shadow()
        .find('div.sign-in-form-footer').find('slot[name=primary-footer-content]')
        .find('span').click()

    cy.get('.radzima', { timeout: 20000 }).should('be.visible')
})

Cypress.Commands.add('logout', () => {
    cy.get('div.ant-dropdown-trigger').trigger('mouseover')
    cy.get('li.ant-dropdown-menu-item').should('be.visible').click()
})

Cypress.Commands.add(
    'waitUntil',
    ({ it, become, delay = 5000, timeout = Cypress.config('defaultCommandTimeout') }) => {
        cy.log('Until start:');
        let lastestResult;
        setTimeout(() => {
            if (typeof become === 'function') {
                expect(become(lastestResult), `${become}`).to.be.true;
            } else {
                expect(lastestResult).to.be.equal(become);
            }
        }, timeout);

        const retry = () => {
            let originalResult = it();

            if (!Cypress.isCy(originalResult)) {
                originalResult = cy.wrap(originalResult, { log: false });
            }

            originalResult.then(result => {
                lastestResult = result;
                let checkResult = false;
                if (typeof become === 'function') {
                    checkResult = become(result);
                } else {
                    checkResult = (result == become);
                }

                if (checkResult) {
                    cy.log(`Until end: "${result}" is expected`);
                    return cy.wrap(result, { log: false });
                } else {
                    cy.log(`Until retry: "${result}" is not expected`);
                    cy.wait(delay);
                    return retry();
                }
            });
        };

        return retry();
    }
);