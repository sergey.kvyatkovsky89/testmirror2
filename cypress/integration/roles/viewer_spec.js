import {
    getDeletionRequest, testObject, deleteObjectApi,
    getCreationRequest, findObject, openObjects, refreshObjectsList
} from '../../support/object.js';
import * as locators from '../../support/locators';

describe('Verify the restrictions for user with Viewer role', () => {

    before('setUp', () => {
        cy.login('viewer')
        cy.request(getDeletionRequest(testObject.name))
            .then((response) => {
                var items = response.body.data.listObjects.items;
                // delete all found test objects
                items.forEach((item) => {
                    deleteObjectApi(item.id);
                })
                // add test object
                cy.request(getCreationRequest())
                    .then((response) => {
                        cy.log(response.body.data.createObject.id);
                        openObjects();
                        refreshObjectsList();
                    });
            });

    })

    it('test inability UI elements for user', () => {

        const ADD_BTN = '.ant-page-header-heading-extra > :nth-child(2)';
        const DEL_BTN = '.ant-page-header-heading-extra > .ant-popover-disabled-compatible-wrapper > .ant-btn';
        // Objects page
        cy.get(':nth-child(6) > .ant-btn').should('have.attr', 'disabled');
        findObject(testObject.name);
        cy.get(':nth-child(1) > a > .ant-btn').click();
        cy.get('.ant-btn-group > :nth-child(1)').should('have.attr', 'disabled');
        cy.get('.ant-btn-primary').should('have.attr', 'disabled');
        cy.get('#object-form-true_name').should('have.attr', 'disabled');
        cy.get('#object-form-true_description').should('have.attr', 'class')
            .and('contain', 'disabled');
        cy.get('#object-form-true_origin').should('have.attr', 'disabled');
        cy.get('#object-form-true_lon').should('have.attr', 'disabled');
        cy.get('#object-form-true_lat').should('have.attr', 'disabled');
        cy.get('#object-form-true_address').should('have.attr', 'disabled');
        cy.get('#object-form-true_permissions').should('have.attr', 'disabled');
        cy.get('#object-form-true_notes').should('have.attr', 'disabled');
        cy.get('#object-form-true_belongsTo').should('have.attr', 'disabled');
        cy.xpath("//input[@type='file']/parent::span").should('have.attr', 'class')
            .and('contain', 'disabled');

        // Gategories page
        cy.get(locators.MENU_LINKS[2]).click();
        cy.get(ADD_BTN).should('have.attr', 'disabled');
        cy.get(DEL_BTN).should('have.attr', 'disabled');
        cy.xpath("//tr/td[4]//div/div[1]/span/button").each((item) => {
            cy.wrap(item).should('have.attr', 'disabled');
        })
        cy.xpath("//tr/td[4]//div/div[3]/span/button").each((item) => {
            cy.wrap(item).should('have.attr', 'disabled');
        })
        cy.xpath("//tr[1]/td[4]//div/div[2]/button[1]").click();
        cy.get('#category-form_name').should('have.attr', 'disabled');
        cy.get('#category-form_singularName').should('have.attr', 'disabled');
        cy.get('#category-form_icon').should('have.attr', 'disabled');
        cy.get('#category-form_icon').should('have.attr', 'disabled');
        cy.xpath("//input[@type='file']/parent::span").should('have.attr', 'class')
            .and('contain', 'disabled');
        cy.get('.ant-btn-primary').should('have.attr', 'disabled');
        cy.get('.ant-modal-footer > :nth-child(1) > span').click();

        // Important to know page
        cy.get(locators.MENU_LINKS[3]).click();
        cy.get(ADD_BTN).should('have.attr', 'disabled');
        cy.get(DEL_BTN).should('have.attr', 'disabled');
        cy.xpath("//tr/td[4]//div/div[2]/span/button").each((item) => {
            cy.wrap(item).should('have.attr', 'disabled');
        })
        cy.xpath("//tr[1]/td[4]//div/div[1]/button").click();
        cy.get('#permission-form_name').should('have.attr', 'disabled');
        cy.get('#permission-form_key').should('have.attr', 'disabled');
        cy.get('.ant-btn-primary').should('have.attr', 'disabled');
        cy.get('.ant-modal-footer > :nth-child(1) > span').click();

        // Tools
        cy.get(locators.MENU_LINKS[4]).click();
        cy.xpath("//a[@href='#/tools/import']/ancestor::li[1]").should('have.attr', 'aria-disabled')
            .and('equal', 'true');

        // Resevations page
        cy.get(locators.MENU_LINKS[7]).click();
        cy.get(locators.MENU_LINKS[8]).click();
        cy.get(ADD_BTN).should('have.attr', 'disabled');
        cy.get(DEL_BTN).should('have.attr', 'disabled');
        cy.xpath("//tr/td[3]//div/div/span/button").each((item) => {
            cy.wrap(item).should('have.attr', 'disabled');
        })

        // Users
        /*cy.xpath("//a[@href='#/users']/ancestor::li[1]").should('have.attr', 'aria-disabled')
            .and('equal', 'true');*/
    })

})