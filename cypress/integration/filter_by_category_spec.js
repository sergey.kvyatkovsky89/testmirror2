import {
    openObjects, selectCategory
} from '../support/object.js';
import * as locators from '../support/locators';

describe('EPMEDUTOUR-805 | Verify that objects list is filtered by category', () => {

    const SIGHTS = ['Памятники войны', 'Замки, дворцы, усадьбы', 'Другие памятники архитектуры',
        'Музеи', 'Памятники природы', 'Церкви, костёлы, монастыри'];
    const PROTECTED_AREAS = 'Заповедные территории';

    before('setUp', () => {

        cy.login('admin')
        openObjects();
        selectCategory(PROTECTED_AREAS);
    })

    it('test filtering of Objects list by categories', () => {

        const categoryLocator = '.ant-list-item-meta-description > .object-description > div';

        cy.get(categoryLocator).each((item) => {
            cy.wrap(item).should('contain.text', PROTECTED_AREAS)
        });

        var newSet = SIGHTS.slice();
        newSet.push(PROTECTED_AREAS);
        cy.get(locators.CATEGORY_ITEM).contains('Достопримечательности').click();

        cy.get(categoryLocator).each((item) => {
            cy.wrap(item).invoke('text').should('be.oneOf', newSet);
        });

        cy.get(':nth-child(1) > .ant-select-selection-item > .ant-select-selection-item-remove')
            .click();

        cy.get(categoryLocator).each((item) => {
            cy.wrap(item).invoke('text').should('be.oneOf', SIGHTS);
        });
    })

})