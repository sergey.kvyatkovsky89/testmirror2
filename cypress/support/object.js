import * as locators from '../support/locators';

const API_URL = 'https://vy4estrvdnetbncg4mewqn3h5q.appsync-api.eu-central-1.amazonaws.com/graphql';
const API_KEY = 'da2-dbjmm7wianexvbpwv3jirt2jcq';
const QUERY_ALL_OBJECTS = 'query getObjects { listObjects { items { id name } } }';
const QUERY_OBJECT_BY_NAME =
    `query getObjectByName { listObjects(filter: {name: {eq: "{0}"}}) { items { id name } } }`;
const QUERY_OBJECTS_BY_CATEGORY =
    `query getObjectsByCategory { listObjects(filter: {categoryId: {eq: "{0}"}}) { items { id name } } }`;
const testObjectImageUrl = 'imagePlaceholder';

export const testObject = {
    name: 'Нулевой километр',
    categoryId: 'b332ccee-d27a-4a51-8349-ac271767c503',
    longitude: '31.013354',
    latitude: '52.423965',
    description: 'This is a test object',
    status: 'draft',
    address: 'площадь Ленина',
    photoFile: 'images/zero-point.png'
}

const fields = 'address area { coordinates type } author category { cover createdAt ' +
    'fields icon id name parent updatedAt } categoryId cover createdAt description ' +
    'duration governanceType id images include { nextToken } length location { lat ' +
    'lon } name notes origin owner permissions { nextToken } status updatedAt url';

const QUERY_CREATE_TEST_OBJECT = `mutation createObject {
  createObject(input: {categoryId: "${testObject.categoryId}",
                       location: {lat: ${testObject.latitude}, lon: ${testObject.longitude}},
                       name: "${testObject.name}",
                       description: "${testObject.description}",
                       status: ${testObject.status},
                       images: ["${testObjectImageUrl}"],
                       cover: "",
                       address: "${testObject.address}"}) { ${fields} } }`

export function format() {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }
    return s;
}

export function openObjects() {
    cy.log('open Objects page');
    cy.get('a[href="#/"]').click();
    cy.get('a[href="#/object"]').click();
    waitListOfObjectsAppear();
}

export function findObject(name) {
    cy.log('find the Object - ' + name);
    cy.get('a[href="#/object"]').click();
    cy.get(locators.SEARCH_FIELD).should('be.enabled', { timeout: 10000 }).clear().type(name);
}

export function isElementPresent(locator) {
    return new Promise((resolve, reject) => {
        cy.get('body').then($body => {
            if ($body.find(locator, { timeout: 20000 }).length > 0) {
                resolve();
            } else {
                reject();
            }
        })
    })
}

export function waitListOfObjectsAppear() {
    cy.get(locators.OBJECT_LIST_ITEM_TITLE, { timeout: 30000 });
}

export function addTestObject() {
    openObjects();
    cy.log('add new Object - ' + testObject.name);
    cy.get('button').contains('Добавить').trigger('mouseover');
    /*cy.get('li.ant-dropdown-menu-item.ant-dropdown-menu-item-only-child')
        .contains('Экскурсии', { timeout: 20000 }).click()*/
    cy.xpath(`//li//span[text()='Достопримечательности']`).click({ force: true });
    cy.xpath(`//li//span[text()='Другие памятники архитектуры']`).click({ force: true });
    // 
    cy.get('#object-form-false_name').type(testObject.name);
    cy.get('.public-DraftStyleDefault-block').type(testObject.description);
    cy.get('#object-form-false_lon').type(testObject.longitude);
    cy.get('#object-form-false_lat').type(testObject.latitude);
    cy.get('#object-form-false_address').type(testObject.address);
    //cy.get('#object-form-false_length').type('50');
    //cy.get('#object-form-false_duration').type('60')
    cy.get('.ant-upload-drag > .ant-upload')
        .attachFile(testObject.photoFile, { subjectType: 'drag-n-drop' });
    cy.get('.ant-btn-primary').click();
    waitListOfObjectsAppear();
    refreshObjectsList();
    cy.log('new Object is added');
}

export function setDescription(objectName, description) {
    cy.get('a > .ant-btn').click();
    cy.get('.public-DraftStyleDefault-block').clear().type(description);
    cy.get('.ant-btn-primary').click();
    cy.get('ul.ant-list-items', { timeout: 20000 }).should('be.visible');
}

export function deleteObjectUi(objectName) {
    findObject(objectName);
    cy.get('li.ant-list-item > button').click();
    cy.get('.ant-popover-buttons > .ant-btn-primary').click();
    cy.get('.ant-input').should('be.enabled', { timeout: 10000 }).clear();
    waitListOfObjectsAppear();
    refreshObjectsList();
    cy.wait(4000);
}

export function importObjects() {
    const fileToImport = 'import.xlsx';
    cy.get(locators.MENU_LINKS[4]).click();
    cy.get(locators.MENU_LINKS[5]).click();
    cy.get(locators.CATEGORY_SELECTOR).click();
    cy.contains('Исторические места').click();
    cy.get('input[type="file"]')
        .attachFile(fileToImport);
    cy.log('file for import is attached');
}

export function exportObjects() {
    cy.get(locators.MENU_LINKS[4]).click();
    cy.get(locators.MENU_LINKS[6]).click();
    cy.get(locators.CATEGORY_SELECTOR).click();
    cy.contains('Заповедные территории', { timeout: 30000 }).click();
    cy.get('.ant-export-icon').click();
}

export function deleteIfExists(objectName) {
    cy.request(getDeletionRequest(objectName))
        .then((response) => {
            var items = response.body.data.listObjects.items;
            // delete all found objects
            items.forEach((item) => {
                deleteObjectApi(item.id);
            })
        });
}

export function refreshObjectsList() {
    cy.contains('Обновить').click();
    cy.get('ul.ant-list-items', { timeout: 10000 }).should('be.visible');
}

export function setStatus(status) {
    cy.get(locators.STATUS_FIELD).click();
    cy.get(locators.STATUS_ITEM).contains(status).click();
}

export function selectCategory(category) {
    cy.get(locators.CATEGORY_FIELD).click();
    cy.get(locators.CATEGORY_ITEM).contains(category).click();
}

export function changeCategory(category) {
    findObject(testObject.name);
    cy.get(locators.EDIT_OBJECT_BTN).click();
    cy.get('.ant-page-header-heading-sub-title > .ant-dropdown-trigger').click();
    cy.xpath(`//li//span[text()='${category}']`).click({ force: true });
    cy.get('.ant-btn-primary').click();
}

export function changeStatus(status) {
    findObject(testObject.name);
    cy.get(locators.EDIT_OBJECT_BTN).click();
    cy.get('.ant-btn-icon-only').click();
    cy.xpath(`//li//span[text()='${status}']`).click({ force: true });
    cy.get('.ant-btn-primary').click();
}

export function changeSubCategory(category, subCategory) {
    findObject(testObject.name);
    cy.get(locators.EDIT_OBJECT_BTN).click();
    cy.get('.ant-page-header-heading-sub-title > .ant-dropdown-trigger').click();
    cy.xpath(`//li//span[text()='${category}']`).click({ force: true });
    cy.xpath(`//li//span[text()='${subCategory}']`).click({ force: true });
    cy.get('.ant-btn-primary').click();
}

/*export function deleteObjectsByName(name) {
    cy.request({
        method: 'POST',
        url: API_URL,
        headers: {
            'x-api-key': API_KEY
        },
        body: { 'query': format(QUERY_OBJECT_BY_NAME, name) }
    }).then((response) => {
        var items = response.body.data.listObjects.items;
        items.forEach((item) => {
            deleteObjectApi(item.id);
        })
    })
}*/

export function getDeletionRequest(name) {
    return {
        method: 'POST',
        url: API_URL,
        headers: { 'x-api-key': API_KEY },
        body: { 'query': format(QUERY_OBJECT_BY_NAME, name) }
    }
}

export function deleteObjectApi(id) {
    const QUERY_DELETE_OBJECT_BY_ID =
        `mutation delObject { deleteObject(input: {id: "${id}"}) { id } }`;
    cy.request({
        method: 'POST',
        url: API_URL,
        headers: {
            'x-api-key': API_KEY
        },
        body: { 'query': QUERY_DELETE_OBJECT_BY_ID }
    }).then((response) => {
        expect(response.status).to.eq(200);
    })
}

export function getCreationRequest() {
    cy.log(QUERY_CREATE_TEST_OBJECT);
    return {
        method: 'POST',
        url: API_URL,
        headers: { 'x-api-key': API_KEY },
        body: { 'query': QUERY_CREATE_TEST_OBJECT }
    }
}

export function getAllObjectsRequest() {
    cy.log(QUERY_ALL_OBJECTS);
    return {
        method: 'POST',
        url: API_URL,
        headers: { 'x-api-key': API_KEY },
        body: { 'query': QUERY_ALL_OBJECTS }
    }
}

export function getObjectsByCategory(id) {
    return {
        method: 'POST',
        url: API_URL,
        headers: { 'x-api-key': API_KEY },
        body: { 'query': format(QUERY_OBJECTS_BY_CATEGORY, id) }
    }
}