import { testObject, addTestObject, findObject, getDeletionRequest, deleteObjectApi }
    from '../support/object.js';
import { OBJECT_LIST_ITEM_TITLE } from '../support/locators';

describe('EPMEDUTOUR-735/-887 | Verify that the object is created', () => {

    before('setUp', () => {

        cy.login('admin');
        cy.request(getDeletionRequest(testObject.name))
            .then((response) => {
                var items = response.body.data.listObjects.items;
                // delete all found test objects
                items.forEach((item) => {
                    deleteObjectApi(item.id);
                })
                // add test object
                addTestObject();
                findObject(testObject.name);
            });
    })

    it('test Creation of new Object', () => {

        cy.get(OBJECT_LIST_ITEM_TITLE).should('contain.text', testObject.name);
    })

})