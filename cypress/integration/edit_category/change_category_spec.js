import {
    changeCategory, getDeletionRequest, testObject, deleteObjectApi,
    changeSubCategory, getCreationRequest, openObjects, refreshObjectsList
} from '../../support/object.js';
import * as locators from '../../support/locators';

describe('EPMEDUTOUR-1940 | Verify that the selected category/sub-category is applied after clicking on Save button', () => {

    const category1 = 'Экскурсии';
    const category2 = 'Достопримечательности';
    const subCategory = 'Музеи';
    before('setUp', () => {

        cy.login('admin')
        cy.request(getDeletionRequest(testObject.name))
            .then((response) => {
                var items = response.body.data.listObjects.items;
                // delete all found test objects
                items.forEach((item) => {
                    deleteObjectApi(item.id);
                });
                // add test object
                cy.request(getCreationRequest())
                    .then((response) => {
                        cy.log(response.body.data.createObject.id);
                        openObjects();
                        refreshObjectsList();
                        changeCategory(category1);
                    });
            });
    })

    it('test', () => {

        cy.get('.object-description > div', {timeout: 20000}).should('have.text', category1);

        changeSubCategory(category2, subCategory);
        cy.get('.object-description > div').should('have.text', subCategory);

    })

})