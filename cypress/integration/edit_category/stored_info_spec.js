import {
    changeCategory, getDeletionRequest, testObject, deleteObjectApi,
    changeSubCategory, getCreationRequest, openObjects, refreshObjectsList
} from '../../support/object.js';
import * as locators from '../../support/locators';

describe('EPMEDUTOUR-1948 | Verify that information previously filled in old fields is stored in DB', () => {

    const category1 = 'Экскурсии';
    const category2 = 'Достопримечательности';
    const subCategory = 'Музеи';
    before('setUp', () => {

        cy.login('admin')
        cy.request(getDeletionRequest(testObject.name))
            .then((response) => {
                var items = response.body.data.listObjects.items;
                // delete all found test objects
                items.forEach((item) => {
                    deleteObjectApi(item.id);
                });
                // add test object
                cy.request(getCreationRequest())
                    .then((response) => {
                        cy.log(response.body.data.createObject.id);
                        openObjects();
                        refreshObjectsList();
                        changeCategory(category1);
                    });
            });
    })

    it('test', () => {

        cy.get(locators.EDIT_OBJECT_BTN).click();
        cy.get('#object-form-true_duration').type('2');
        cy.get('.ant-btn-primary').click();

        changeSubCategory(category2, subCategory);

        changeCategory(category1);

        cy.get(locators.EDIT_OBJECT_BTN).click();
        cy.get('#object-form-true_duration').should('have.value', '2');

    })

})