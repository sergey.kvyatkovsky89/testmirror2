import {
    deleteObjectUi, testObject, openObjects, getDeletionRequest, getCreationRequest,
    deleteObjectApi, refreshObjectsList, findObject, getAllObjectsRequest
} from '../support/object.js';
import * as locators from '../support/locators';

describe('EPMEDUTOUR-737/-889 | Verify that the object can be deleted', () => {

    before('setUp', () => {

        cy.login('admin');
        cy.request(getDeletionRequest(testObject.name))
            .then((response) => {
                var items = response.body.data.listObjects.items;
                // delete all found test objects
                items.forEach((item) => {
                    deleteObjectApi(item.id);
                });
                // add test object
                cy.request(getCreationRequest())
                    .then((response) => {
                        cy.log(response.body.data.createObject.id);
                        openObjects();
                        refreshObjectsList();
                        // delete test object
                        deleteObjectUi(testObject.name);
                    });
            });
    })

    it('test Object deletion', () => {

        cy.get(locators.OBJECT_LIST_ITEM_TITLE).each((item) => {
            cy.wrap(item).should('not.contain.text', testObject.name);
        });

        findObject(testObject.name);
        cy.get('.ant-empty-description').should('have.text', 'Нет данных');

        cy.request(getAllObjectsRequest())
            .then((response) => {
                var items = response.body.data.listObjects.items;
                items.forEach((item) => {
                    expect(item.name).not.equal(testObject.name);
                })
            })
    })

})