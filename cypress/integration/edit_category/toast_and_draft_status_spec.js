import {
    getDeletionRequest, testObject, deleteObjectApi, changeStatus,
    getCreationRequest, openObjects, refreshObjectsList
} from '../../support/object.js';
import * as locators from '../../support/locators';

describe(`EPMEDUTOUR-1943 / -1944 | Verify that the toast message about changing status to 'Draft' is displayed. 
                                    Verify that the status 'Draft' is applied`, () => {

    const status = 'На утверждении';
    const category = 'Экскурсии';
    before('setUp', () => {

        cy.login('admin')
        cy.request(getDeletionRequest(testObject.name))
            .then((response) => {
                var items = response.body.data.listObjects.items;
                // delete all found test objects
                items.forEach((item) => {
                    deleteObjectApi(item.id);
                });
                // add test object
                cy.request(getCreationRequest())
                    .then((response) => {
                        cy.log(response.body.data.createObject.id);
                        openObjects();
                        refreshObjectsList();
                        changeStatus(status);
                    });
            });
    })

    it('test', () => {

        cy.get(locators.EDIT_OBJECT_BTN, { timeout: 10000 }).click();
        cy.get('.ant-page-header-heading-sub-title > .ant-dropdown-trigger').click();
        cy.xpath(`//li//span[text()='${category}']`).click({ force: true });
        
        cy.get('.ant-notification-notice-message').should('have.text', 'Статус объекта изменен на Черновик')

        cy.get('.ant-btn-primary').click();

        cy.get('.ant-tag').should('have.text', 'Черновик');

    })

})