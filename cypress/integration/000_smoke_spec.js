import { PAGE_TITLE } from '../support/locators';

describe('EPMEDUTOUR-506 | Smoke test', () => {

    before('login', () => {
        cy.login('admin');
    })

    it('test Navigation', () => {
        cy.log('Radzima main page is opened')

        cy.get('.radzima').contains('Radzima Admin')

        cy.get(PAGE_TITLE).should('have.text', 'Обзор')

        cy.get('a[href="#/object"]').click()
        cy.get(PAGE_TITLE).should('have.text', 'Объекты')

        cy.get('a[href="#/category"]').click()
        cy.get(PAGE_TITLE).should('have.text', 'Категории')

        cy.get('a[href="#/permission"]').click()
        cy.get(PAGE_TITLE).should('have.text', 'Важно знать')

        cy.contains('Инструменты').click()
        cy.get('a[href="#/tools/import"]').click()
        cy.get(PAGE_TITLE).should('have.text', 'Импорт')
        cy.get('a[href="#/tools/export"]').click()
        cy.get(PAGE_TITLE).should('have.text', 'Экспорт')

        cy.contains('Справочники').click()
        cy.get('a[href="#/references/reserves"]').click()
        cy.get(PAGE_TITLE).should('have.text', 'Заповедники')

        cy.get('a[href="#/users"]').click()
        cy.get(PAGE_TITLE).should('have.text', 'Пользователи')
    }) 

})