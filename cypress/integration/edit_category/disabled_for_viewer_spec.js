import {
    changeCategory, getDeletionRequest, testObject, deleteObjectApi,
    changeSubCategory, getCreationRequest, openObjects, refreshObjectsList
} from '../../support/object.js';
import * as locators from '../../support/locators';

describe(`EPMEDUTOUR-1949 | Verify that category/sub-category changing isn't available for Viewer`, () => {

    before('setUp', () => {
        cy.login('viewer')
        openObjects();
        cy.get(':nth-child(1) > a > .ant-btn').click();
    })

    it('test', () => {

        cy.xpath("//span[@class='ant-dropdown-trigger obj-category-dropdown']")
            .should('have.attr', 'disabled');

    })

})