const allFields = ['Pасположение',
    'Название',
    'Адрес',
    'Важно знать',
    'Описание',
    'Имя из справочника',
    'Включает',
    'Источник',
    'Официальный сайт',
    'Заметки',
    'Автор',
    'Принадлежит к',
    'Протяженность, км',
    'Продолжительность'];

export function reservationsFields() {
    var newSet = allFields.slice();
    newSet.splice(newSet.indexOf('Название'), 1);
    newSet.splice(newSet.indexOf('Протяженность, км'), 1);
    newSet.splice(newSet.indexOf('Продолжительность'), 1);
    return newSet;
}

export function historicalPlacesFields() {
    var newSet = allFields.slice();
    newSet.splice(newSet.indexOf('Имя из справочника'), 1);
    newSet.splice(newSet.indexOf('Протяженность, км'), 1);
    newSet.splice(newSet.indexOf('Официальный сайт'), 1);
    newSet.splice(newSet.indexOf('Продолжительность'), 1);
    return newSet;
}

export function routesFields() {
    var newSet = allFields.slice();
    newSet.splice(newSet.indexOf('Имя из справочника'), 1);
    newSet.splice(newSet.indexOf('Официальный сайт'), 1);
    newSet.splice(newSet.indexOf('Продолжительность'), 1);
    return newSet;
}

export function excursionsFields() {
    var newSet = allFields.slice();
    newSet.splice(newSet.indexOf('Pасположение'), 1);
    newSet.splice(newSet.indexOf('Имя из справочника'), 1);
    newSet.splice(newSet.indexOf('Протяженность, км'), 1);
    newSet.splice(newSet.indexOf('Адрес'), 1);
    newSet.splice(newSet.indexOf('Важно знать'), 1);
    return newSet;
}

export function sightsFields() {
    var newSet = allFields.slice();
    newSet.splice(newSet.indexOf('Имя из справочника'), 1);
    newSet.splice(newSet.indexOf('Протяженность, км'), 1);
    newSet.splice(newSet.indexOf('Продолжительность'), 1);
    return newSet;
}