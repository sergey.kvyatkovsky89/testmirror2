import {
    reservationsFields, historicalPlacesFields, routesFields, excursionsFields,
    sightsFields
} from '../../support/categories';
import * as locators from '../../support/locators';

const CATEGORIES = ['Заповедные территории', 'Маршруты', 'Экскурсии', 'Достопримечательности'];

describe('EPMEDUTOUR-1897 | Verify set of fields for each category', () => {

    function test(fields) {
        var amount = 0;
        cy.xpath("//div[@class='ant-select-selection-overflow']/div/span/span")
            .each((item) => {
                cy.wrap(item).invoke('text').should('be.oneOf', fields);
                amount++;
            }).then(() => {
                expect(amount).to.be.equal(fields.length);
            });
        cy.get('.ant-modal-close-x').click();
    }

    before('setUp', () => {
        cy.loginProd('viewerProd')
        cy.xpath(locators.TOTAL_OBJECTS, { timeout: 40000 }).should('be.visible');
    })

    it('test', () => {

        cy.get(locators.MENU_LINKS[2]).click();

        // Categories
        var counter = 0;
        cy.xpath("//tbody/tr/td[3]")
            .each((item) => {
                cy.wrap(item).invoke('text').should('be.oneOf', CATEGORIES);
                counter++;
            }).then(() => {
                expect(counter).to.be.equal(CATEGORIES.length);
            });

        // Reservations
        cy.xpath(`//td[text()='${CATEGORIES[0]}']/parent::tr//div/div[2]/button[1]`).click();
        test(reservationsFields());

        // Routes
        cy.xpath(`//td[text()='${CATEGORIES[1]}']/parent::tr//div/div[2]/button[1]`).click();
        test(routesFields());

        // Excursions
        cy.xpath(`//td[text()='${CATEGORIES[2]}']/parent::tr//div/div[2]/button[1]`).click();
        test(excursionsFields());

        // Sights
        cy.xpath(`//td[text()='${CATEGORIES[3]}']/parent::tr//div/div[2]/button[1]`).click();
        test(sightsFields());

    })

    after('after', () => {
        cy.logout();
    })
})