/// <reference types="cypress" />

const registerReportPortalPlugin = require('@reportportal/agent-js-cypress/lib/plugin');
const fs = require('fs');
const XLSX = require('xlsx');

module.exports = (on, config) => {

    registerReportPortalPlugin(on);

    on('task', {
        getDownloadedFile(dir) {
            return new Promise((resolve, reject) => {
                fs.readdir(dir, (err, files) => {
                    if (err) {
                        return reject(err)
                    }

                    resolve(dir + '/' + files[0])
                })
            })
        },
        countFiles(folderName) {
            return new Promise((resolve, reject) => {
                fs.readdir(folderName, (err, files) => {
                    if (err) {
                        return reject(err)
                    }

                    resolve(files.length)
                })
            })
        },
        readXlsxFile({ file }) {
            const buf = fs.readFileSync(file);
            const workbook = XLSX.read(buf, { type: 'buffer' });

            const rows = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);

            return rows
        },
        delXlsxFile({ file }) {
            try {
                fs.unlinkSync(file);
            } catch (err) {
                console.log(err);
            }
            
            return null
        },
    })

    // Add test-users from environment to objects
    // admin
    var username = process.env.ADMIN_USER
    var password = process.env.ADMIN_PASS

    if (!username) {
        throw new Error(`missing ADMIN_USER environment variable`)
    }
    if (!password) {
        throw new Error(`missing ADMIN_PASS environment variable`)
    }
    // form an object
    // from the spec use Cypress.env('admin') to access it
    config.env.admin = {
        username, password,
    }

    // viewer
    username = process.env.VIEWER_USER
    password = process.env.VIEWER_PASS

    if (!username) {
        throw new Error(`missing VIEWER_USER environment variable`)
    }
    if (!password) {
        throw new Error(`missing VIEWER_PASS environment variable`)
    }

    config.env.viewer = {
        username, password,
    }

    // content editor
    username = process.env.EDITOR_USER
    password = process.env.EDITOR_PASS

    if (!username) {
        throw new Error(`missing EDITOR_USER environment variable`)
    }
    if (!password) {
        throw new Error(`missing EDITOR_PASS environment variable`)
    }

    config.env.editor = {
        username, password,
    }

    // approver
    username = process.env.APPROVER_USER
    password = process.env.APPROVER_PASS

    if (!username) {
        throw new Error(`missing APPROVER_USER environment variable`)
    }
    if (!password) {
        throw new Error(`missing APPROVER_USER environment variable`)
    }

    config.env.approver = {
        username, password,
    }

    // viewer prod
    username = process.env.VIEWER_PROD_USER
    password = process.env.VIEWER_PROD_PASS

    if (!username) {
        throw new Error(`missing VIEWER_PROD_USER environment variable`)
    }
    if (!password) {
        throw new Error(`missing VIEWER_PROD_USER environment variable`)
    }

    config.env.viewerProd = {
        username, password,
    }

    // make sure to return the updated `config` object
    return config
}