import { exportObjects, getObjectsByCategory } from '../support/object.js';
import { MENU_LINKS } from '../support/locators';

describe('EPMEDUTOUR-801 | Verify that the objects are exported', () => {

    var exportedFile;

    before('setUp', () => {

        cy.login('admin');
        
        cy.get(MENU_LINKS[2]).click();
        exportObjects();
        cy.wait(3000);
        // todo: create 'cypress/downloads' folder
        cy.waitUntil({
            it: () => cy.task('countFiles', 'cypress/downloads'),
            become: 1,
            timeout: 5000,
            delay: 300,
        });
        //

        cy.task('getDownloadedFile', 'cypress/downloads', { timeout: 15000 })
            .then((fileName) => {
                exportedFile = fileName;
                cy.log(fileName);
            });
    })

    it('test Export of Objects', () => {

        var objectNames = [];

        cy.task('readXlsxFile', { file: exportedFile }).then((json) => {
            json.forEach((item) => {
                cy.log(item.name);
                objectNames.push(item.name);
            })
            cy.request(getObjectsByCategory('6047a51e85272e884685376f'))
                .then((response) => {
                    var items = response.body.data.listObjects.items;
                    for (var i = 0; i < objectNames.length; i++) {
                        expect(items[i].name).equal(objectNames[i]);
                    }
                })
        })
        
    })

})