import {
    getDeletionRequest, testObject, deleteObjectApi,
    findObject, getCreationRequest, openObjects, refreshObjectsList
} from '../support/object.js';
import * as locators from '../support/locators';

describe('EPMEDUTOUR-891 | Verify that the search is working', () => {

    before('setUp', () => {

        cy.login('admin')
        cy.request(getDeletionRequest(testObject.name))
            .then((response) => {
                var items = response.body.data.listObjects.items;
                // delete all found test objects
                items.forEach((item) => {
                    deleteObjectApi(item.id);
                });
                // add test object
                cy.request(getCreationRequest())
                    .then((response) => {
                        cy.log(response.body.data.createObject.id);
                        openObjects();
                        refreshObjectsList();
                    });
            });
    })

    it('test Seach Object functionality', () => {
        findObject(testObject.name);
        cy.get(locators.OBJECT_LIST_ITEM_TITLE).should('have.text', testObject.name);
        cy.get(locators.SEARCH_FIELD).clear();
        findObject('non-existent object');
        cy.get('.ant-empty-description').should('have.text', 'Нет данных');
    })

})