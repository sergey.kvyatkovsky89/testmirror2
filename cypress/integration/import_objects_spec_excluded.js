import {
    testObject, findObject, getDeletionRequest, deleteObjectApi, getCreationRequest,
    importObjects, openObjects, deleteIfExists, refreshObjectsList
} from '../support/object.js';
import * as locators from '../support/locators';

describe('EPMEDUTOUR-800 | Verify that imported objects are appears in list', () => {

    const SECOND_OBJECT_NAME = 'Object for test import #2';
    const THIRD_OBJECT_NAME = 'Object for test import #3';
    const FIRST_OBJECT_DESC = 'test object #1';
    const IMPORT_SUCCESS_TEXT = '2 объект(-ов) импортировано успешно';
    const UPDATE_SUCCESS_TEXT = '1 объект(-ов) было обновленно успешно';

    before('setUp', () => {

        cy.login('admin');

        openObjects();

        deleteIfExists(SECOND_OBJECT_NAME);
        deleteIfExists(THIRD_OBJECT_NAME);

        cy.request(getDeletionRequest(testObject.name))
            .then((response) => {
                var items = response.body.data.listObjects.items;
                // delete all found test objects
                items.forEach((item) => {
                    deleteObjectApi(item.id);
                })
                // add test object
                cy.request(getCreationRequest())
                    .then((response) => {
                        cy.log(response.body.data.createObject.id);
                        // import objects
                        importObjects();
                    });
            });
    })

    it('test Import of Objects', () => {

        cy.get('.ant-notification-notice', { timeout: 15000 }).should('be.visible');

        cy.get(':nth-child(1) > .ant-typography > strong')
            .should('contain.text', IMPORT_SUCCESS_TEXT);
        cy.get(':nth-child(2) > .ant-typography > strong')
            .should('contain.text', UPDATE_SUCCESS_TEXT);

        cy.get('.ant-notification-close-x').click();

        openObjects();
        refreshObjectsList();

        findObject(SECOND_OBJECT_NAME);
        cy.get(locators.OBJECT_LIST_ITEM_TITLE).should('have.text', SECOND_OBJECT_NAME);

        openObjects();

        findObject(THIRD_OBJECT_NAME);
        cy.get(locators.OBJECT_LIST_ITEM_TITLE).should('have.text', THIRD_OBJECT_NAME);

        openObjects();

        findObject(testObject.name);
        cy.get(locators.EDIT_OBJECT_BTN).click();
        cy.get(locators.DESCRIPTION_FIELD).should('have.text', FIRST_OBJECT_DESC);
    })

})