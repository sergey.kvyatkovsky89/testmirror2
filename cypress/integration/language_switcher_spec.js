import * as locators from '../support/locators';
import { format } from '../support/object.js';

function selectLanguage(lng) {
    cy.get('.locale-toggle > .ant-select > .ant-select-selector').click();
    cy.get(`div[label=${lng}]`).click();
    cy.get('.ant-select-selection-item').should('contain.text', lng);
}

const menuItemsEn = ['Overview', 'Objects', 'Categories', 'Important to know', 'Tools', 'Import', 'Export',
    'References', 'National parks', 'Users'];
const menuItemsRu = ['Обзор', 'Объекты', 'Категории', 'Важно знать', 'Инструменты', 'Импорт', 'Экспорт',
    'Справочники', 'Заповедники', 'Пользователи'];
const cardTitlesEn = ['Total objects', 'Published', 'Waiting for approval', 'Draft', 'Archived'];
const cardTitlesRu = ['Всего объектов', 'Опубликовано', 'На утверждении', 'Черновик', 'Архив'];
const statusPlaceholderRu = 'Пожалуйста, выберите Статус';
const statusPlaceholderEn = 'Please select Status';
const categoryPlaceholderRu = 'Пожалуйста, выберите Категорию';
const categoryPlaceholderEn = 'Please select Category';

describe('EPMEDUTOUR-802 | Verify the language of titles of control elements', () => {

    before('login', () => {
        cy.login('admin');
    })

    it('test Language switcher', () => {
        // Menu items
        cy.get(locators.MENU_LINKS[4]).click();
        cy.get(locators.MENU_LINKS[7]).click();
        selectLanguage('EN');
        for (var i = 0; i < locators.MENU_LINKS.length; i++) {
            cy.get(locators.MENU_LINKS[i]).should('have.text', menuItemsEn[i])
        }
        selectLanguage('RU');
        for (var i = 0; i < locators.MENU_LINKS.length; i++) {
            cy.get(locators.MENU_LINKS[i]).should('have.text', menuItemsRu[i])
        }

        // Overview page
        selectLanguage('EN');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsEn[0])
        for (var i = 0; i < cardTitlesEn.length; i++) {
            var title = format(locators.CARD_TITLE, i + 1);
            cy.get(title).should('have.text', cardTitlesEn[i]);
        }
        selectLanguage('RU');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsRu[0])
        for (var i = 0; i < cardTitlesRu.length; i++) {
            var title = format(locators.CARD_TITLE, i + 1);
            cy.get(title).should('have.text', cardTitlesRu[i]);
        }
        
        // Objects page
        cy.get(locators.MENU_LINKS[1]).click();
        selectLanguage('EN');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsEn[1]);

        cy.xpath(`//div[contains(@class,'main-header')]/div[2]//span[contains(@class,'placeholder')]`)
            .should('have.text', statusPlaceholderEn);
        cy.xpath(`//div[contains(@class,'main-header')]/div[3]//span[contains(@class,'placeholder')]`)
            .should('have.text', categoryPlaceholderEn);
        cy.xpath(`//div[contains(@class,'main-header')]/div[4]//input`).should('have.attr', 'placeholder')
            .and('equal', 'Search');
        cy.xpath(`//div[contains(@class,'main-header')]/div[5]//span`)
            .should('have.text', 'Refresh');
        cy.xpath(`//div[contains(@class,'main-header')]/div[6]//span`)
            .should('have.text', 'Add');
        cy.get('.ant-tag').each((item) => {
            cy.wrap(item).invoke('text').should('be.oneOf', cardTitlesEn);
        });
        
        selectLanguage('RU');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsRu[1]);
        cy.xpath(`//div[contains(@class,'main-header')]/div[2]//span[contains(@class,'placeholder')]`)
            .should('have.text', statusPlaceholderRu);
        cy.xpath(`//div[contains(@class,'main-header')]/div[3]//span[contains(@class,'placeholder')]`)
            .should('have.text', categoryPlaceholderRu);
        cy.xpath(`//div[contains(@class,'main-header')]/div[4]//input`).should('have.attr', 'placeholder')
            .and('equal', 'Поиск');
        cy.xpath(`//div[contains(@class,'main-header')]/div[5]//span`)
            .should('have.text', 'Обновить');
        cy.xpath(`//div[contains(@class,'main-header')]/div[6]//span`)
            .should('have.text', 'Добавить');
        cy.get('.ant-tag').each((item) => {
            cy.wrap(item).invoke('text').should('be.oneOf', cardTitlesRu);
        });

        // Categories page
        cy.get(locators.MENU_LINKS[2]).click();
        selectLanguage('EN');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsEn[2]);
        cy.xpath(locators.ADD_BTN).should('have.text', 'Add');
        cy.xpath(locators.DEL_BTN).should('have.text', 'Delete');
        cy.xpath(locators.REFRESH_BTN).should('have.text', 'Refresh');
        cy.get('.ant-table-thead > tr > :nth-child(3)').should('have.text', 'Name');
        cy.get('.ant-table-thead > tr > :nth-child(4)').should('have.text', 'Action');

        selectLanguage('RU');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsRu[2]);
        cy.xpath(locators.ADD_BTN).should('have.text', 'Добавить');
        cy.xpath(locators.DEL_BTN).should('have.text', 'Удалить');
        cy.xpath(locators.REFRESH_BTN).should('have.text', 'Обновить');
        cy.get('.ant-table-thead > tr > :nth-child(3)').should('have.text', 'Название');
        cy.get('.ant-table-thead > tr > :nth-child(4)').should('have.text', 'Действие');

        // Permissions page
        cy.get(locators.MENU_LINKS[3]).click();
        selectLanguage('EN');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsEn[3]);
        cy.xpath(locators.ADD_BTN).should('have.text', 'Add');
        cy.xpath(locators.DEL_BTN).should('have.text', 'Delete');
        cy.xpath(locators.REFRESH_BTN).should('have.text', 'Refresh');
        cy.get('.ant-table-thead > tr > :nth-child(2)').should('have.text', 'Name');
        cy.get('.ant-table-thead > tr > :nth-child(3)').should('have.text', 'Key');
        cy.get('.ant-table-thead > tr > :nth-child(4)').should('have.text', 'Action');

        selectLanguage('RU');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsRu[3]);
        cy.xpath(locators.ADD_BTN).should('have.text', 'Добавить');
        cy.xpath(locators.DEL_BTN).should('have.text', 'Удалить');
        cy.xpath(locators.REFRESH_BTN).should('have.text', 'Обновить');
        cy.get('.ant-table-thead > tr > :nth-child(2)').should('have.text', 'Название');
        cy.get('.ant-table-thead > tr > :nth-child(3)').should('have.text', 'Идентификатор');
        cy.get('.ant-table-thead > tr > :nth-child(4)').should('have.text', 'Действие');

        // Import page
        cy.get(locators.MENU_LINKS[4]).click();
        cy.get(locators.MENU_LINKS[5]).click();
        selectLanguage('EN');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsEn[5]);
        cy.get('.ant-upload-text').should('have.text', 'Drop files to attach, or browse');
        cy.get('.ant-select-selector > .ant-select-selection-placeholder')
            .should('have.text', categoryPlaceholderEn);

        selectLanguage('RU');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsRu[5]);
        cy.get('.ant-upload-text').should('have.text', 'Перенесите файлы, чтобы прикрепить, или обзор');
        cy.get('.ant-select-selector > .ant-select-selection-placeholder')
            .should('have.text', categoryPlaceholderRu);

        // Export page
        cy.get(locators.MENU_LINKS[6]).click();
        selectLanguage('EN');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsEn[6]);
        cy.get('.ant-export-text').should('have.text', 'Click to export data');
        cy.get('.ant-select-selector > .ant-select-selection-placeholder')
            .should('have.text', categoryPlaceholderEn);

        selectLanguage('RU');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsRu[6]);
        cy.get('.ant-export-text').should('have.text', 'Нажмите, чтобы экспортировать данные');
        cy.get('.ant-select-selector > .ant-select-selection-placeholder')
            .should('have.text', categoryPlaceholderRu);

        // Reserves page
        cy.get(locators.MENU_LINKS[7]).click();
        cy.get(locators.MENU_LINKS[8]).click();
        selectLanguage('EN');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsEn[8]);
        cy.xpath(locators.ADD_BTN).should('have.text', 'Add');
        cy.xpath(locators.DEL_BTN).should('have.text', 'Delete');
        cy.xpath(locators.REFRESH_BTN).should('have.text', 'Refresh');

        selectLanguage('RU');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsRu[8]);
        cy.xpath(locators.ADD_BTN).should('have.text', 'Добавить');
        cy.xpath(locators.DEL_BTN).should('have.text', 'Удалить');
        cy.xpath(locators.REFRESH_BTN).should('have.text', 'Обновить');

        // Users page
        cy.get(locators.MENU_LINKS[9]).click();
        selectLanguage('EN');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsEn[9]);
        cy.xpath(locators.ADD_BTN).should('have.text', 'Add');
        cy.xpath(locators.REFRESH_BTN).should('have.text', 'Refresh');
        cy.xpath("//tr[1]/th[1]/div/span[1]", { timeout: 20000 }).should('have.text', 'Email');
        cy.xpath("//tr[1]/th[2]").should('have.text', 'Enabled');
        cy.xpath("//tr[1]/th[3]").should('have.text', 'Account Status');
        cy.xpath("//tr[1]/th[4]").should('have.text', 'Email Verified');
        cy.xpath("//tr[1]/th[5]").should('have.text', 'Updated');
        cy.xpath("//tr[1]/th[6]").should('have.text', 'Created');

        selectLanguage('RU');
        cy.get(locators.PAGE_TITLE).should('have.text', menuItemsRu[9]);
        cy.xpath(locators.ADD_BTN).should('have.text', 'Добавить');
        cy.xpath(locators.REFRESH_BTN).should('have.text', 'Обновить');
        cy.xpath("//tr[1]/th[1]/div/span[1]").should('have.text', 'Электронная почта');
        cy.xpath("//tr[1]/th[2]").should('have.text', 'Активирован');
        cy.xpath("//tr[1]/th[3]").should('have.text', 'Статус аккаунта');
        cy.xpath("//tr[1]/th[4]").should('have.text', 'Электронная почта проверена');
        cy.xpath("//tr[1]/th[5]").should('have.text', 'Дата обновления');
        cy.xpath("//tr[1]/th[6]").should('have.text', 'Дата cоздания');

    }) 

})