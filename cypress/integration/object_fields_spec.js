import {
    getDeletionRequest, testObject, deleteObjectApi,
    findObject, getCreationRequest, openObjects, refreshObjectsList
} from '../support/object.js';
import * as locators from '../support/locators';

describe('EPMEDUTOUR-799 | Verify that all object fields are displayed', () => {

    before('setUp', () => {

        cy.login('admin')
        cy.request(getDeletionRequest(testObject.name))
            .then((response) => {
                var items = response.body.data.listObjects.items;
                // delete all found test objects
                items.forEach((item) => {
                    deleteObjectApi(item.id);
                });
                // add test object
                cy.request(getCreationRequest())
                    .then((response) => {
                        cy.log(response.body.data.createObject.id);
                        openObjects();
                        refreshObjectsList();
                        findObject(testObject.name);
                        cy.get(locators.EDIT_OBJECT_BTN).click();
                    });
            });
    })

    it('test All Object fields', () => {
        cy.get('#object-form-true_name').should('be.visible');        
        cy.get(locators.DESCRIPTION_FIELD).should('be.visible');
        cy.get('#object-form-true_origin').should('be.visible');
        cy.get('#object-form-true_lon').should('be.visible');
        cy.get('#object-form-true_lat').should('be.visible');
        cy.xpath("//label[contains(text(),'Важно знать')]/../..//div[@class='ant-select-selection-overflow']")
            .should('be.visible');
        cy.get('#object-form-true_notes').scrollIntoView().should('be.visible');
        cy.get('#object-form-true_author').scrollIntoView().should('be.visible');
        cy.xpath("//label[contains(text(),'Принадлежит к')]/../..//div[@class='ant-select-selection-overflow']")
            .scrollIntoView().should('be.visible');
        cy.get('.ant-upload-drag-container').should('be.visible');
    })

})