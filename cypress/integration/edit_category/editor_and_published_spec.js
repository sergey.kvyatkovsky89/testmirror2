import {
    changeCategory, getDeletionRequest, testObject, deleteObjectApi, setStatus,
    changeSubCategory, getCreationRequest, openObjects, refreshObjectsList
} from '../../support/object.js';
import * as locators from '../../support/locators';

describe(`EPMEDUTOUR-1979 /-1991 | Verify that category/sub-category changing isn't available for Contenteditor and Published objects`, () => {

    const category2 = 'Достопримечательности';
    const subCategory = 'Музеи';

    before('setUp', () => {
        cy.login('editor')
        cy.request(getDeletionRequest(testObject.name))
            .then((response) => {
                var items = response.body.data.listObjects.items;
                // delete all found test objects
                items.forEach((item) => {
                    deleteObjectApi(item.id);
                });
                // add test object
                cy.request(getCreationRequest())
                    .then((response) => {
                        cy.log(response.body.data.createObject.id);
                        openObjects();
                        refreshObjectsList();
                        changeSubCategory(category2, subCategory);
                    });
            });
    })

    it('test', () => {

        cy.get('.object-description > div').should('have.text', subCategory);

        cy.get(locators.SEARCH_FIELD).should('be.enabled', { timeout: 10000 }).clear();
        setStatus('Опубликовано');
        cy.get(':nth-child(1) > a > .ant-btn').click();
        cy.xpath("//span[@class='ant-dropdown-trigger obj-category-dropdown']")
            .should('have.attr', 'disabled');

    })

})