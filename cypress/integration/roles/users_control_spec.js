import * as locators from '../../support/locators';

describe('Verify that Admin can manage users', () => {

    const testUserEmail = 'auto-test@epam.com';

    function checkUser (userEmail) {
        var users = [];
        cy.xpath("//tr/td[1]/a", { timeout: 20000 }).each((item) => {
            var text = item.text();
            users.push(text);
            cy.log(text);
            cy.log('index - ' + users.toString().indexOf(userEmail));
        }).then(() => {
            if (users.toString().indexOf(userEmail) >= 0) {
                cy.xpath(`//tr/td[1]/a[text()='${testUserEmail}']`).click();
                deleteUser(testUserEmail);
            }
        });
    };

    function deleteUser(userEmail) {
        cy.xpath("//div/button[3]/span").click();
        cy.xpath("//div/button[4]/span").click();
        cy.get('.ant-btn-primary').click();
    };

    before('setUp', () => {

        cy.login('admin');
        cy.get(locators.MENU_LINKS[9]).click();
        checkUser(testUserEmail);
    })

    it('test ability to add, block, enable and delete user', () => {

        //cy.get(locators.MENU_LINKS[9]).click();
        // Add
        cy.get('.ant-page-header-heading-extra > :nth-child(1)').click();
        cy.get('#user-form_email').type(testUserEmail);
        cy.get('.ant-modal-footer > .ant-btn-primary').click();
        // Refresf
        cy.get('.ant-page-header-heading-extra > :nth-child(2)', { timeout: 20000 }).click();   

        cy.xpath(`//tr/td[1]/a[text()='${testUserEmail}']`).should('be.visible');

        // User screen
        cy.xpath(`//tr/td[1]/a[text()='${testUserEmail}']`).click();
        cy.xpath("//div/button[1]/span").should('have.text', 'Добавить в группу');
        cy.xpath("//div/button[2]/span").should('have.text', 'Cбросить пароль');
        cy.xpath("//div/button[3]/span", { timeout: 20000 })
            .should('have.text', 'Заблокировать пользователя');

        cy.get(':nth-child(1) > strong').should('contain.text', 'Группы');
        cy.get(':nth-child(3) > strong').should('contain.text', 'Активирован');
        cy.get(':nth-child(6) > strong').should('contain.text', 'Статус аккаунта');
        cy.get(':nth-child(9) > strong').should('contain.text', 'Последнее изменение');
        cy.get(':nth-child(12) > strong').should('contain.text', 'Дата cоздания');
        cy.get(':nth-child(15) > strong').should('contain.text', 'Электронная почта');

        cy.xpath("//span[@role='img' and @class='anticon anticon-check-circle']")
            .should('be.visible');

        // Disable user
        cy.xpath("//div/button[3]/span").click();
        cy.xpath("//div/button[4]/span").should('have.text', 'Удалить пользователя');

        cy.xpath("//span[@role='img' and @class='anticon anticon-close-circle']")
            .should('be.visible');

        // Activate user
        cy.xpath("//div/button[3]/span").click();

        cy.xpath("//span[@role='img' and @class='anticon anticon-check-circle']")
            .should('be.visible');

        // Delete user
        deleteUser(testUserEmail);
        //Refresf
        cy.get('.ant-page-header-heading-extra > :nth-child(2)').click();

        // check user absent
        var users = [];
        cy.xpath("//tr/td[1]/a", { timeout: 20000 }).each((item) => {
            users.push(item.text());
        }).then(() => {
            expect(users.toString().indexOf(testUserEmail)).equal(-1);
        })

    })

})