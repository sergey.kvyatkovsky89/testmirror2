import {
    getDeletionRequest, testObject, deleteObjectApi,
    findObject, getCreationRequest, openObjects, refreshObjectsList
} from '../support/object.js';
import * as locators from '../support/locators';

describe('EPMEDUTOUR-1958 | Verify that user can go to the linked objects', () => {

    before('setUp', () => {

        cy.login('admin')
        cy.request(getDeletionRequest(testObject.name))
            .then((response) => {
                var items = response.body.data.listObjects.items;
                // delete all found test objects
                items.forEach((item) => {
                    deleteObjectApi(item.id);
                });
                // add test object
                cy.request(getCreationRequest())
                    .then((response) => {
                        cy.log(response.body.data.createObject.id);
                        openObjects();
                        refreshObjectsList();
                        findObject(testObject.name);
                        cy.get(locators.EDIT_OBJECT_BTN).click();
                    });
            });
    })

    it('test', () => {
        const firstItem = "//div[@class='rc-virtual-list-holder-inner']/div[1]";
        cy.get('#object-form-true_belongsTo').click();
        cy.xpath(firstItem + '/div')
            .invoke('text').then((text) => {
                var objectName = text.trim();
                cy.xpath(firstItem).click();
                cy.xpath(firstItem + '/div/a').should('have.attr', 'target').and('equal', '_blank');
                cy.get('.ant-select-selection-item-content > a').then(function ($a) {
                    const href = $a.prop('href')
                    // and now visit the href directly
                    cy.visit(href);
                    cy.get(locators.PAGE_TITLE).should('contain.text', objectName);
                })
            })
    })

})