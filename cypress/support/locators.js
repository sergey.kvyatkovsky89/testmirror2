export const PAGE_TITLE = '.ant-page-header-heading-title';
export const EDIT_OBJECT_BTN = 'a > .ant-btn';
export const DESCRIPTION_FIELD = '.public-DraftStyleDefault-block';
export const CATEGORY_FIELD = ':nth-child(3) > .ant-input-group > .ant-select > .ant-select-selector > .ant-select-selection-overflow';
export const CATEGORY_ITEM = '.ant-select-tree-title';
export const STATUS_FIELD = ':nth-child(2) > .ant-input-group > .ant-select > .ant-select-selector > .ant-select-selection-overflow';
export const STATUS_ITEM = '.ant-select-item-option-content';
export const SEARCH_FIELD = '.ant-input';
export const CARD_TITLE = `:nth-child({0}) > .ant-card > .ant-card-body > .ant-statistic > .ant-statistic-title > .ant-row > .ant-col`;
export const TOTAL_OBJECTS = "//div[contains(@class,'ant-row-middle')]/div[1]//span[@class='ant-statistic-content-value']/span";
export const MENU_LINKS = ['a[href="#/"]', 'a[href="#/object"]', 'a[href="#/category"]', 'a[href="#/permission"]',
    ':nth-child(5) > .ant-menu-submenu-title > :nth-child(2)', 'a[href="#/tools/import"]', 'a[href="#/tools/export"]',
    ':nth-child(6) > .ant-menu-submenu-title > :nth-child(2)', 'a[href="#/references/reserves"]', 'a[href="#/users"]'];
export const ADD_BTN = `//span[contains(@class,'ant-page-header-heading-extra')]/button[1]/span[2]`;
export const DEL_BTN = `//span[contains(@class,'ant-page-header-heading-extra')]/span/button//span[2]`;
export const REFRESH_BTN = `//span[contains(@class,'ant-page-header-heading-extra')]//button[2]//span[2]`;
export const OBJECT_LIST_ITEM_TITLE = '.ant-list-item-meta-title';
export const CATEGORY_SELECTOR = '.ant-page-header-heading-extra > .ant-select > .ant-select-selector';