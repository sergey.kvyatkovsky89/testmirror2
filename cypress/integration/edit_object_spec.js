import {
    setDescription, getDeletionRequest, testObject, deleteObjectApi,
    findObject, getCreationRequest, openObjects, refreshObjectsList
} from '../support/object.js';
import * as locators from '../support/locators';

describe('EPMEDUTOUR-736/-888 | Verify that the object can be edited', () => {

    const description = 'Updated Description';

    before('setUp', () => {

        cy.login('admin')
        cy.request(getDeletionRequest(testObject.name))
            .then((response) => {
                var items = response.body.data.listObjects.items;
                // delete all found test objects
                items.forEach((item) => {
                    deleteObjectApi(item.id);
                });
                // add test object
                cy.request(getCreationRequest())
                    .then((response) => {
                        cy.log(response.body.data.createObject.id);
                        openObjects();
                        refreshObjectsList();
                        // updqate description
                        findObject(testObject.name);
                        setDescription(testObject.name, description);
                    });
            });
    })

    it('test Edit Object description', () => {

        cy.log('check edited description');
        openObjects();
        refreshObjectsList();
        findObject(testObject.name);
        cy.get(locators.EDIT_OBJECT_BTN).click();
        cy.get(locators.DESCRIPTION_FIELD).should('have.text', description);
    })

})