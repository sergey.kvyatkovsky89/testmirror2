import * as locators from '../../support/locators';

const CATEGORIES = ['Заповедные территории', 'Исторические места', 'Маршруты',
    'Экскурсии', 'Достопримечательности'];

const SIGHTS = ['Памятники войны', 'Замки, дворцы, усадьбы', 'Другие памятники архитектуры',
    'Музеи', 'Памятники природы', 'Церкви, костёлы, монастыри'];

const ROUTES = ['Водные маршруты', 'Пешие маршруты', 'Велосипедные маршруты'];


describe('EPMEDUTOUR-2013 | Verify set of sub-categories', () => {

    function test(fields) {
        var amount = 0;
        cy.xpath("//tr[contains(@class,'ant-table-expanded-row')]//tr/td[2]")
            .each((item) => {
                cy.wrap(item).invoke('text').should('be.oneOf', fields);
                amount++;
            }).then(() => {
                expect(amount).to.be.equal(fields.length);
            });
    }

    before('setUp', () => {
        cy.loginProd('viewerProd');
        cy.xpath(locators.TOTAL_OBJECTS, { timeout: 40000 }).should('be.visible');
    })

    it('test', () => {

        cy.get(locators.MENU_LINKS[2]).click();

        // Sights
        cy.xpath(`//td[text()='${CATEGORIES[4]}']/parent::tr/td[1]/button`).click();
        test(SIGHTS);
        cy.xpath(`//td[text()='${CATEGORIES[4]}']/parent::tr/td[1]/button`).click();

        cy.reload();

        // Routes
        cy.xpath(`//td[text()='${CATEGORIES[2]}']/parent::tr/td[1]/button`).click();
        test(ROUTES);
        
    })

    after('after', () => {
        cy.logout();
    })
})